package com.tw;

public class NormalParkingBoy extends ParkingBoyBase {
    // TODO: You can change anything within this range including which method to override.
    // <-start-
    @Override
    public ParkingTicket park(Car car) {
        ParkingResult parkingResult = new ParkingResult("The parking lot is full.");
        ParkingTicket parkingTicket = new ParkingTicket();
        for (int i = 0; i < this.getParkingLots().size(); i++) {
            if (parkingResult.isSuccess() == true) {
                break;
            }
            ParkingLot parkingLot = this.getParkingLots().get(i);
            if (parkingLot.getAvailableParkingPosition() == 0) {
                continue;
            }
            parkingTicket = parkingLot.park(car);
            parkingResult = new ParkingResult(parkingTicket);
        }
        if (parkingResult.isSuccess() == false) {
            this.setLastErrorMessage(parkingResult.getMessage());
            return null;
        }
        return parkingTicket;
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        FetchingResult fetchingResult = new FetchingResult("Unrecognized parking ticket.");
        if (ticket == null) {
            this.setLastErrorMessage("Please provide your parking ticket.");
            return null;
        }
        boolean fetchStatus = false;
        Car car = new Car();
        for (int i = 0; i < this.getParkingLots().size(); i++) {
            if (fetchingResult.isSuccess() == true) {
                break;
            }
            ParkingLot parkingLot = this.getParkingLots().get(i);
            if (parkingLot.containsTicket(ticket)) {
                car = parkingLot.fetch(ticket);
                fetchingResult = new FetchingResult(car);
            }
        }
        if (fetchingResult.isSuccess() == false) {
            this.setLastErrorMessage(fetchingResult.getMessage());
            return null;
        }
        return car;
    }
    // --end->
}
