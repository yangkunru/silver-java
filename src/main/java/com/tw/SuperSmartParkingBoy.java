package com.tw;

public class SuperSmartParkingBoy extends ParkingBoyBase {
    // TODO: You can change anything within this range including which method to override.
    // <-start-
    @Override
    public ParkingTicket park(Car car) {
        double availablePositionRate = 0;
        for (int i = 0; i < this.getParkingLots().size(); i++) {
            if (availablePositionRate < (double) this.getParkingLots().get(i).getAvailableParkingPosition() / this.getParkingLots().get(i).getCapacity()) {
                availablePositionRate = (double) this.getParkingLots().get(i).getAvailableParkingPosition() / this.getParkingLots().get(i).getCapacity();
            }
        }
        boolean parkStatus = false;
        ParkingTicket parkingTicket = new ParkingTicket();
        for (int i = 0; i < this.getParkingLots().size(); i++) {
            if (parkStatus == true) {
                break;
            }
            ParkingLot parkingLot = this.getParkingLots().get(i);
            if (parkingLot.getAvailableParkingPosition() == 0) {
                continue;
            }
            if ((double) parkingLot.getAvailableParkingPosition() / this.getParkingLots().get(i).getCapacity() == availablePositionRate) {
                parkingTicket = parkingLot.park(car);
                parkStatus = true;
            }
        }
        if (parkStatus == false) {
            this.setLastErrorMessage("The parking lot is full.");
            return null;
        }
        return parkingTicket;
    }

    @Override
    public Car fetch(ParkingTicket ticket) {
        if (ticket == null) {
            this.setLastErrorMessage("Please provide your parking ticket.");
            return null;
        }
        boolean fetchStatus = false;
        Car car = new Car();
        for (int i = 0; i < this.getParkingLots().size(); i++) {
            if (fetchStatus == true) {
                continue;
            }
            ParkingLot parkingLot = this.getParkingLots().get(i);
            if (parkingLot.containsTicket(ticket)) {
                car = parkingLot.fetch(ticket);
                fetchStatus = true;
            }
        }
        if (fetchStatus == false) {
            this.setLastErrorMessage("Unrecognized parking ticket.");
            return null;
        }
        return car;
    }
    // --end->
}
